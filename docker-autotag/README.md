# Description

The `docker-autotag` function automatically adds alias tags to new images
on [Docker Hub][] according to predefined rules.

[Docker Hub]: https://hub.docker.com/

# Configuration

The `docker-autotag` function requires configuration in the application settings.

 1. Set `QUEUE_STORAGE` to a connection string for an Azure Storage account.
    It may be the one used by the Azure Functions app.
 2. Set `DOCKER_AUTOTAG_QUEUE` to a unique queue name such as `docker-autotag`.
 3. Set `DOCKERUSERNAME` to the user name of a Docker Hub account.
 4. Set `DOCKERPASSWORD` to the password of the same account.
    This will be changed in later revisions for security.

# See also

## Docker Registry documentation

[Docker Registry HTTP API V2](https://docs.docker.com/registry/spec/api/).

[Docker Registry Authentication](https://docs.docker.com/registry/spec/auth/).

Docker Image Manifest V2, [Schema 1](https://docs.docker.com/registry/spec/manifest-v2-1/)
and [Schema 2](https://docs.docker.com/registry/spec/manifest-v2-2/).
