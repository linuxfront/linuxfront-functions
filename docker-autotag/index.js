// docker-autotag - add alias tags to new images on Docker Hub
// Copyright (C) 2017 Kaz Nishimura
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//
// SPDX-License-Identifier: MIT

const request = require("request");

// Tagging rules.
const rules = {
    "kazssym/wildfly": {
    },
    "kazssym/derby": {
    },
    "kazssym/wordpress": {
        "php7.0": ["latest"],
    },
};

const API_BASE = "https://registry-1.docker.io/v2/";

const MANIFEST_V2 = "application/vnd.docker.distribution.manifest.v2+json";
const MANIFEST_LIST_V2 = "application/vnd.docker.distribution.manifest.list.v2+json";

// List of media types to be requested for manifests.
// It will be joined to build an 'Accept' request header.
const MANIFEST_ACCEPT = [
    MANIFEST_LIST_V2,
    MANIFEST_V2,
    "*/*;q=0.1",
];

function addTagsToImage(context, name, reference, tags)
{
    "use strict";

    let session = {
        start: function () {
            let url = API_BASE + name + "/manifests/" + reference;
            context.log.info("Pulling %s:%s", name, reference);

            let req = request.get(url, {
                    headers: {
                        "accept": MANIFEST_ACCEPT.join(","),
                    },
                }, (error, response, body) => {
                    if (error != null) {
                        context.done(error);
                        return;
                    }

                    let mediaType = response.headers["content-type"];
                    switch (response.statusCode) {
                    case 200:
                        this.pushManifest(body, mediaType, tags.slice());
                        break;
                    case 401:
                        if (this.accessToken != null) {
                            context.log(response);
                            context.done(new Error("Access token rejected"));
                            return;
                        }
                        this.authenticate(response, this.start.bind(this));
                        break;
                    default:
                        context.log.error("Unexpected status code %d: %j",
                            response.statusCode, JSON.parse(body));
                        context.done(new Error("Unexpected status code"));
                    }
                });
            if (this.accessToken != null) {
                req.auth(null, null, true, this.accessToken);
            }
            req.end();
        },
        pushManifest: function (manifest, mediaType, tags) {
            if (tags.length == 0) {
                context.done();
                return;
            }
            if (mediaType == null) {
                mediaType = "application/json";
            }

            let tag = tags.shift();
            let url = API_BASE + name + "/manifests/" + tag;
            context.log.info("Pushing %s:%s", name, tag);

            let req = request.put(url, {
                    headers: {
                        "content-type": mediaType,
                    },
                    body: manifest,
                }, (error, response, body) => {
                    if (error != null) {
                        context.done(error);
                        return;
                    }

                    switch (response.statusCode) {
                    case 201:
                        this.pushManifest(manifest, mediaType, tags);
                        break;
                    // case 401:
                    //     if (this.accessToken != null) {
                    //         context.log("Access token error");
                    //         context.log(response);
                    //         context.done();
                    //         return;
                    //     }
                    //     this.authenticate(response, this.start.bind(this));
                    //     break;
                    default:
                        context.log.error("Unexpected status code %d: %j",
                            response.statusCode, JSON.parse(body));
                        context.done(new Error("Unexpected status code"));
                    }
                });
            if (this.accessToken != null) {
                req.auth(null, null, true, this.accessToken);
            }
            req.start();
        },
        authenticate: function (response, retry) {
            let challenge = response.headers["www-authenticate"];

            let scheme = challenge.match(/^(\w+)/)[1];
            if (scheme.toLowerCase() != "bearer") {
                context.log.error("Invalid auth-scheme '%s'", scheme);
                context.done(new Error("Invalid auth-scheme"));
                return;
            }

            let url = challenge.match(/[ ,]realm *= *"([^"]*)"/i)[1];
            let service = challenge.match(/[ ,]service *= *"([^"]*)"/i)[1];
            let username = process.env["DOCKERUSERNAME"];
            let password = process.env["DOCKERPASSWORD"];

            let req = request.get(url, {
                    auth: {
                        username: username,
                        password: password,
                    },
                    json: true,
                }, (error, response, body) => {
                    if (error != null) {
                        context.done(error);
                        return;
                    }

                    switch (response.statusCode) {
                    case 200:
                        this.accessToken = body["access_token"];
                        retry();
                        break;
                    default:
                        context.log.error("Unexpected status code %d: %j",
                            response.statusCode, body);
                        context.done(new Error("Unexpected status code"));
                    }
                });
            req.qs({
                    "service": service,
                    "scope": "repository:" + name + ":pull,push",
                }).start();
        },
    };
    session.start();
}

module.exports = function (context, item) {
    "use strict";

    context.log.verbose("Queue item %j", item);

    let repoName = item["repository"]["repo_name"];
    if (repoName in rules) {
        let tag = item["push_data"]["tag"];
        if (tag in rules[repoName]) {
            addTagsToImage(context, repoName, tag, rules[repoName][tag]);
        }
        else {
            context.log.info("Unknown tag '%s:%s'", repoName, tag);
            context.done();
        }
    }
    else {
        context.log.error("Unknown repository '%s'", repoName);
        context.done();
    }
}
