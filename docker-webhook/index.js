// docker-webhook - queues push data from Docker Hub for deferred processing
// Copyright (C) 2017 Kaz Nishimura
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//
// SPDX-License-Identifier: MIT

module.exports = function (context, data)
{
    "use strict";

    // Log the JSON object for diagnostics.
    context.log.verbose("Received %j", data);

    // Make an error response by default.
    context.bindings.res = {
        status: 400,
        body: {
            "message": "Invalid content",
        },
    };

    // Check the JSON object structure.
    let repoName = null;
    let tag = null;
    if (data != null) {
        if ("repository" in data) {
            repoName = data["repository"]["repo_name"];
        }
        if ("push_data" in data) {
            tag = data["push_data"]["tag"];
        }
    }

    if (repoName != null && tag != null) {
        context.bindings.queueItems = data;

        // Make a response with status 202 as the request is just to be
        // accepted and queued.
        context.bindings.res = {
            status: 202,
            body: {
                "message": "Queued",
            },
        };
    }

    context.done();
}
