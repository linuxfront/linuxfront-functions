# Description

This function queues push data from [Docker Hub][]
to an [Azure Queue storage][] for deferred processing
by the `docker-autotag` function.

[Docker Hub]: https://hub.docker.com/
[Azure Queue storage]: https://azure.microsoft.com/en-us/services/storage/queues/

# Notes

It would take longer than expected as a webhook if `require` was used
for modules.

# See also

[Docker Hub webhooks](https://docs.docker.com/docker-hub/webhooks/
"Webhooks for automated builds").
Although this document describes webhooks for automated builds,
they also work for normal repositories.

[Azure Storage Documentation](https://docs.microsoft.com/en-us/azure/storage/).
