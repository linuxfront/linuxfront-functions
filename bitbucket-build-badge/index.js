// bitbucket-build-badge - build status badge generator for Bitbucket Cloud
// Copyright (C) 2017 Kaz Nishimura
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//
// SPDX-License-Identifier: MIT

const url = require("url");
const https = require("https");

// Base URI of the Bitbucket API 2.0.
const API_BASE = "https://api.bitbucket.org/2.0/";

// Base URI for the Shields.io custom badges.
const BADGE_BASE = "https://img.shields.io/badge/";

// Badge URI when an error is detected.
const BADGE_ERROR = BADGE_BASE + "build-inaccessible-lightgray.svg";

// Badge URI when no status record is found.
const BADGE_NO_STATUS = BADGE_BASE + "build-not_available-lightgray.svg";

// Badge URIs for build states.
const badges = {
    "SUCCESSFUL": BADGE_BASE + "build-successful-brightgreen.svg",
    "FAILED": BADGE_BASE + "build-failed-red.svg",
    "INPROGRESS": BADGE_BASE + "build-in_progress-lightgray.svg",
    "STOPPED": BADGE_BASE + "build-stopped-yellow.svg",
};

// Value of 'max-age' in the response 'Cache-Control' header.
const MAX_AGE = 600;

// Makes a 'GET' request for a JSON value.
function get(options, callback)
{
    "use strict";
    if (typeof options === "string") {
        options = url.parse(options);
    }
    options.method = "GET";

    if (options.headers == null) {
        options.headers = {};
    }
    options.headers["User-Agent"] = "bitbucket-build-badge";
    if (options.headers["Accept"] == null) {
        // We want an 'application/json' content unless otherwise specified.
        options.headers["Accept"] = "application/json";
    }

    let request = https.request(options);
    if (callback != null) {
        request.on("error",
            (error) => {
                callback(error);
            }
        );
        request.on("response",
            (response) => {
                let content = "";
                response.on("data",
                    (data) => {
                        content = content + data;
                    }
                )
                response.on("end",
                    () => {
                        switch (response.statusCode) {
                        case 200:
                        case 401:
                        case 404:
                            callback(null, JSON.parse(content),
                                response.statusCode);
                            break;
                        default:
                            callback(new Error(response.statusMessage));
                        }
                    }
                );
            }
        );
    }
    request.end();
}

// Get paginated values as an 'Array'.
function getPaginated(uri, callback, values)
{
    "use strict";
    if (values == null) {
        values = [];
    }
    get(uri,
        (error, object, statusCode) => {
            if (error != null) {
                callback(error);
                return;
            }
            if (object["type"] == "error") {
                callback(new Error(object["error"]["message"]));
                return;
            }
            values = values.concat(object["values"]);

            let next = object["next"];
            if (next != null) {
                // We have more values to get.
                getPaginated(next, callback, values);
            }
            else {
                callback(null, values);
            }
        }
    );
}

module.exports = function (context, request)
{
    "use strict";

    let flow = {
        // Serves a build status badge for a branch.
        // The branch name may be 'null' for the main branch.
        serveBadge: function (repositoryName, branchName)
        {
            if (branchName == null) {
                // TODO: Get the main branch name from the repository.
                branchName = "default";
            }

            let branchUri = API_BASE + "repositories/" + repositoryName
                + "/refs/branches/" + branchName;
            get(branchUri,
                (error, object, statusCode) => {
                    if (error != null || statusCode != 200) {
                        if (error != null) {
                            context.log.error(error);
                        }

                        this.sendRedirect(BADGE_ERROR);
                        context.done();
                        return;
                    }

                    let hash = object["target"]["hash"];
                    this.serveBadgeForCommit(repositoryName, hash);
                }
            );
        },

        // Serves a build status badge for a commit.
        serveBadgeForCommit: function (repositoryName, hash)
        {
            let statusesUri = API_BASE + "repositories/" + repositoryName
                + "/commit/" + hash + "/statuses";
            // TODO: Handle pagination.
            getPaginated(statusesUri,
                (error, values) => {
                    if (error != null) {
                        context.log.error(error);
                        this.sendRedirect(BADGE_ERROR);
                        context.done();
                        return;
                    }

                    let lastBuild = values.filter(
                        // We are interested in 'build' objects only.
                        (object) => object["type"] == "build")
                    .map((build) => {
                            // We want these properties as 'Date'.
                            for (let key of ["created_on", "updated_on"]) {
                                if (key in build) {
                                    build[key] = new Date(build[key]);
                                }
                            }
                            return build;
                        })
                    .reduce((lastBuild, build) => {
                            // We want the last one.
                            if (lastBuild == null ||
                                lastBuild["updated_on"] < build["updated_on"]) {
                                lastBuild = build;
                            }
                            return lastBuild;
                        }, null);
                    context.log.verbose("lastBuild = %j", lastBuild);

                    let badgeUri;
                    if (lastBuild != null) {
                        badgeUri = badges[lastBuild["state"]];
                    }
                    if (badgeUri == null) {
                        badgeUri = BADGE_NO_STATUS;
                    }
                    this.sendRedirect(badgeUri);
                    context.done();
                }
            );
        },

        // Sends a redirect response.
        sendRedirect: function (location) {
            context.bindings.response = {
                status: 302,
                headers: {
                    "Location": location,
                    "Cache-Control": "max-age=" + MAX_AGE.toFixed(),
                },
                body: null,
            };
        },
    };

    // Extracts parameters.
    let owner = context.bindingData["owner"];
    let repository = context.bindingData["repository"];
    let branch = context.bindingData["branch"];

    flow.serveBadge(owner + "/" + repository, branch);
    // We have not done yet.
}
