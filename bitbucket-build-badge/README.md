# Description

This function is a frontend to [Shields.io][] and generates build status badges for [Bitbucket Cloud][].
The actual badge images are served by [Shields.io][].

[Bitbucket Cloud]: https://bitbucket.org/
[Shields.io]: https://shields.io/

# See also

[Bitbucket API version 2.0](https://developer.atlassian.com/bitbucket/api/2/reference/).
