# Description

This [Azure Functions][] app consists of miscellaneous functions for private use
including the followings:

  - `docker-autotag` ([README](docker-autotag/README.md)).
  - `docker-webhook` ([README](docker-webhook/README.md)).
  - `bitbucket-build-badge` ([README](bitbucket-build-badge/README.md)).

This software is provided under the terms and conditions of the [MIT License][MIT].

See the [repository wiki][wiki] for more information.

[![(MIT License)](https://img.shields.io/badge/license-MIT-blue.svg)][MIT]
[![(Open Issues)](https://img.shields.io/bitbucket/issues/linuxfront/functions-azure.svg)][open issues]
[![(Build Status)](https://linuxfront-functions.azurewebsites.net/api/bitbucket/build/linuxfront/functions-azure?branch=master)][pipelines]

[Azure Functions]: https://azure.microsoft.com/en-us/services/functions/
[MIT]: https://opensource.org/licenses/MIT

[Wiki]: https://bitbucket.org/linuxfront/functions-azure/wiki
[Open issues]: https://bitbucket.org/linuxfront/functions-azure/issues?status=new&status=open
[Pipelines]: https://bitbucket.org/linuxfront/functions-azure/addon/pipelines/home

# See also

[Azure Functions Documentation](https://docs.microsoft.com/en-us/azure/azure-functions/).
